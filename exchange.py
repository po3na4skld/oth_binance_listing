from binance.client import Client

from models import SpotAccount, MarginAccount
from base.exceptions import BinanceHttpError
from models import Ticker, BinanceOrder


class BinanceExchange:

    def __init__(self, **kwargs):
        self._key = kwargs.get('key')
        self._secret = kwargs.get('secret')

        self.client = Client(self._key, self._secret)

    def close_deal(self, deal):
        open_order = deal.open_order

        if open_order.side == 'BUY':
            order = self.client.order_market_sell(
                symbol=deal.symbol,
                quantity=open_order.executed_qty
            )
        else:
            order = self.client.create_margin_order(
                symbol=deal.symbol,
                quantity=open_order.executed_qty,
                side='BUY',

            )

        if 'price' not in order:
            raise BinanceHttpError(data=order)

        order = BinanceOrder(**order)
        deal.close_order = order
        return deal, order

    def create_order(self, action, symbol, count,
                     execution_price=None, stop_loss=None, take_profit=None):

        if action not in ['buy', 'sell']:
            raise ValueError('action can be only buy or sell')

        if action == 'buy':
            if execution_price:
                order = self.client.order_limit_buy(
                    symbol=symbol,
                    quantity=count,
                    price=str(execution_price),
                    STOP_LOSS=stop_loss,
                    TAKE_PROFIT=take_profit
                )
            else:
                order = self.client.order_market_buy(
                    symbol=symbol,
                    quantity=count,
                    STOP_LOSS=stop_loss,
                    TAKE_PROFIT=take_profit
                )
        else:
            order = self.client.create_margin_order(
                symbol=symbol,
                quantity=count,
                side='SELL',

            )

        if 'price' not in order:
            raise BinanceHttpError(data=order)

        return BinanceOrder(**order)

    def get_last_data(self, symbol):
        return Ticker(**self.client.get_ticker(symbol=symbol))

    def get_symbol_fee(self, symbol, fee_type='maker'):
        if fee_type not in ['maker', 'taker']:
            raise ValueError('fee_type can be only maker or taker')

        trade_fee = self.client.get_trade_fee(symbol=symbol)['tradeFee']
        return float(trade_fee[0][fee_type]) if trade_fee else None

    def get_asset_balance(self, asset, account_type='spot'):
        if account_type not in ['spot', 'margin']:
            raise ValueError('account_type can be only spot or margin')

        a = self.get_account(account_type)
        a = SpotAccount(**a) if account_type == 'spot' else MarginAccount(**a)

        balance = list([i.free for i in a.assets if i.name == asset])
        return balance[0] if balance else None

    def get_account(self, account_type='spot'):
        if account_type not in ['spot', 'margin']:
            raise ValueError('account_type can be only spot or margin')

        total_balance = self.get_capital(account_type, convert=True)

        if account_type == 'spot':
            return dict(capital=total_balance, **self.client.get_account())
        else:
            return dict(capital=total_balance, **self.client.get_margin_account())

    def get_capital(self, account_type='spot', convert=False):
        response = self.client._request_margin_api(
            'get',
            'accountSnapshot',
            True,
            data={'type': account_type.upper()}
        )

        if response['code'] != 200 or 'snapshotVos' not in response:
            raise BinanceHttpError(data=response['msg'])

        btc_balance = float(response['snapshotVos'][-1]['data']['totalAssetOfBtc'])
        if convert:
            return btc_balance * self.get_last_data('BTCUSDT').last_price

        return btc_balance


if __name__ == '__main__':
    exchange = BinanceExchange(**{'key': '70YQ1gnO5rOXSHQpNtOffLUYa29BBjsKqsVRMuzf3gGMhnurM9Hn2i72PFcxv7p3 ',
                                  'secret': 'e05Fk2fET7dXUjkgN7NEds1LNl7guRvf6lioU3xOF5SmSQNemA2Oixl1TBykEZaz',
                                  })

    # print(exchange.get_last_data('ETHUSDT'))
    print(exchange.get_capital(convert=True))
