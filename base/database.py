from pymongo import MongoClient


class MongoDB:

    def __init__(self, **kwargs):
        self.user = kwargs.get('user', '')
        self.password = kwargs.get('password', '')
        self.ip = kwargs.get('id', 'localhost')
        self.port = kwargs.get('port', 27017)
        self.name = kwargs.get('name', '')

        if not self.name:
            raise ValueError('db name can not be empty')

        if not self.user and not self.password:
            self.client = MongoClient(f"mongodb://{self.ip}:{self.port}")[self.name]
        else:
            self.client = MongoClient(f"mongodb://{self.user}:{self.password}@{self.ip}:{self.port}")[self.name]

    def fetch_all(self, **kwargs):
        """
        Fetch all records from database
        :param kwargs: Dict object with query in it.
            kwargs format:
                'collection': Name of collection.
                    'required': True,
                    'type': str,
                    'example': articles
                'query': Query
                    'required': True,
                    'type': str,
                    'example': {'uuid': 'uuid_example'}
                'sort': a list of (key, direction) pairs specifying the sort order for this query
                    'required': False,
                    'type': list
                    'example': ['field_name', 1)] 1 == ASCENDING, -1 == DESCENDING
                'limit_size': Limit size or page size.
                    'required': False,
                    'type': int,
                    'example': 15
                'page': Page number, 0 - default. From which page return records.
                    'required': False,
                    'type': int,
                    'example': 1
        :return: list of documents.
        """

        collection = kwargs.get('collection', '')
        if not collection:
            raise ValueError('collection field can not be empty')

        query = kwargs.get('query', {})
        return_fields = kwargs.get('return_fields', '')
        sort = kwargs.get('sort', None)

        limit_size = kwargs.get('limit_size', 0)
        skip = kwargs.get('page', 0) * limit_size

        if return_fields:
            result = self.client[collection].find(query, return_fields, sort=sort, limit=limit_size, skip=skip)
        else:
            result = self.client[collection].find(query, sort=sort, limit=limit_size, skip=skip)

        return [doc for doc in result]

    def fetch_one(self, **kwargs):
        """
        Fetch all records from database
        :param kwargs: Dict object with query in it.
            kwargs format:
                'collection': Name of collection.
                    'required': True,
                    'type': str,
                    'example': articles
                'query': Query
                    'required': True,
                    'type': str,
                    'example': {'uuid': 'uuid_example'}
                'sort': a list of (key, direction) pairs specifying the sort order for this query
                    'required': False,
                    'type': list
                    'example': ['field_name', 1)] 1 == ASCENDING, -1 == DESCENDING
        :return: list of documents.
        """

        collection = kwargs.get('collection', '')
        if not collection:
            raise ValueError('collection field can not be empty')

        query = kwargs.get('query', {})
        return_fields = kwargs.get('return_fields', '')
        sort = kwargs.get('sort', None)

        if return_fields:
            result = self.client[collection].find_one(query, return_fields, sort=sort)
        else:
            result = self.client[collection].find_one(query, sort=sort)

        return dict(result) if result else None

    def execute(self, **kwargs):
        """
        Executes a special query
        :param kwargs: Dict object with query in it.
            kwargs format:
                'collection': Name of collection.
                    'required': True,
                    'type': str,
                    'example': articles
                'query': Query
                    'required': True,
                    'type': str,
                    'example': {'uuid': 'uuid_example'}
                'docs': a list of documents to insert or update
                    'required': False,
                    'type': list if many documents, dict if only one,
                    'example': [{'uuid': 'uuid_example}]
        :return: Doesn't returns anything.
        """

        collection = kwargs.get('collection', '')
        if not collection:
            raise ValueError('collection field can not be empty')

        method = kwargs.get('method', '')
        if method not in ['insert', 'update', 'delete']:
            return ValueError(f"Invalid type argument. Must be in {['insert', 'update', 'delete']}")

        query = kwargs.get('query', {})
        docs = kwargs.get('docs', {})

        if method == 'insert':
            if isinstance(docs, list):
                self.client[collection].insert_many(docs)
            else:
                self.client[collection].insert_one(docs)

        elif method == 'update':
            self.client[collection].update_one(query, {'$set': docs})

        elif method == 'delete':
            self.client[collection].delete_many(query)
