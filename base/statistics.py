import numpy as np


def sharpe_ratio(deals, last_price=None):
    """Returns Sharpe Ratio"""
    if not deals:
        return 0

    if last_price:
        mn = np.mean([i.live_returns(last_price) for i in deals])
        std = np.std([i.live_returns(last_price) for i in deals])
    else:
        mn = np.mean([i.returns for i in deals])
        std = np.std([i.returns for i in deals])

    return mn / std


def sortino_ratio(deals, last_price=None):
    """Returns Sortino Ratio"""
    if not deals:
        return 0

    if last_price:
        mn = np.mean([i.live_returns(last_price) for i in deals])
        std_dev = []
        for i in deals:
            if i.is_filled:
                if not i.is_profitable:
                    std_dev.append(i.returns)
            else:
                if i.live_returns(last_price) < 0:
                    std_dev.append(i.live_returns(last_price))

        return mn / np.std(std_dev)

    mn = np.mean([i.returns for i in deals])
    std = np.std([i.returns for i in deals if not i.is_profitable])

    return mn / std


def average_win(deals):
    """Returns average win amount"""
    if not deals:
        return 0

    return np.mean([i.returns for i in deals if i.is_profitable]) if deals else 0


def average_loss(deals):
    """Returns average loss amount"""
    if not deals:
        return 0

    return np.mean([i.returns for i in deals if not i.is_profitable]) if deals else 0


def maximum_win(deals):
    """Returns maximum win amount"""
    if not deals:
        return 0

    return max([i.returns for i in deals]) if deals else 0


def maximum_loss(deals):
    """Returns maximum loss amount"""
    if not deals:
        return 0

    return min([i.returns for i in deals]) if deals else 0


def volatility(deals):
    """Returns volatility"""
    if not deals:
        return 0

    return np.std([i.returns for i in deals]) if deals else 0


def win_rate(deals):
    """Returns win rate"""
    if not deals:
        return 0

    return len([i for i in deals if i.is_profitable]) / len(deals) if deals else 0


def get_metrics(deals, profit, pretty=False):
    """Returns performance metrics as dictionary"""
    metrics = {k: v(deals) for k, v in DEFAULT_METRICS.items()}
    metrics.update({'deals_performed': len(deals), 'profit': profit})
    if pretty:
        info = []
        for field, value in metrics.items():
            show_field = field.replace('_', ' ').capitalize()
            if isinstance(value, float):
                value = round(value, 2)
            info.append(f"{show_field}: {value}")

        return "\n".join(info)

    return metrics


DEFAULT_METRICS = {
    'win_rate': win_rate, 'volatility': volatility,
    'avg_win': average_win, 'avg_loss': average_loss,
    'max_win': maximum_win, 'max_loss': maximum_loss,
    'sortino_ratio': sortino_ratio, 'sharpe_ratio': sharpe_ratio
}
