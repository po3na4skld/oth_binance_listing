from decimal import Decimal
from uuid import uuid4
import datetime


class BaseModel:
    """
    BaseModel class for different models. Realizes some basic functionality.
    :param uuid: uuid, UUID.
    :param created: datetime, Time when model was created.
    :param modified: datetime, Time when model was modified.

    """
    save_fields = []
    update_fields = []
    show_fields = []

    collection_name = ''

    def __init__(self, **kwargs):

        self.save_fields += ['created', 'modified', 'new', 'uuid']
        self.update_fields += ['modified', 'new']

        if not kwargs.get('uuid', ''):
            self.uuid = uuid4().__str__()
            self.new = True
        else:
            self.uuid = kwargs.get('uuid')
            self.new = False

        self.created = kwargs.get('created', '')
        if not self.created:
            self.created = datetime.datetime.now()

        self.modified = kwargs.get('modified', '')
        if not self.modified:
            self.modified = datetime.datetime.now()

    def _modify(self):
        """Modifies the model"""
        self.modified = datetime.datetime.now()

    def to_dict(self, child_object=None, fields=None):
        """Returns dictionary representation of model"""
        if not child_object:
            child_object = self

        result = {}
        for attribute in dir(child_object):
            if attribute.startswith('_'):
                continue

            if attribute in ['id', 'update_fields', 'save_fields']:
                if not fields or attribute not in fields:
                    continue

            if fields and attribute not in fields:
                continue

            attr = getattr(child_object, attribute)

            if callable(attr):
                continue

            if isinstance(attr, datetime.datetime):
                result[attribute] = attr.strftime('%Y-%m-%dT%H:%M:%SZ')
                continue

            if isinstance(attr, datetime.date):
                result[attribute] = attr.strftime('%Y-%m-%d')
                continue

            if isinstance(attr, Decimal):
                if attr == Decimal('0'):
                    result[attribute] = 0
                    continue

                number = str(attr).split('.')
                if len(number) == 2:
                    number[1] = number[1][:2]
                    result[attribute] = float(f'{number[0]}.{number[1]}')
                else:
                    result[attribute] = float(attr)
                continue

            if isinstance(attr, list):
                result[attribute] = []
                if not len(attr):
                    continue

                for row in attr:
                    if isinstance(row, BaseModel):
                        result[attribute].append(self.to_dict(row, row.save_fields))
                    else:
                        result[attribute].append(row)
                continue

            if isinstance(attr, BaseModel):
                result[attribute] = attr.to_dict()
                continue

            result[attribute] = attr

        return result

    def __str__(self):
        fields = self.show_fields if self.show_fields else self.save_fields
        info = []

        for field in fields:
            if getattr(self, field) is None:
                continue

            show_field = field.replace('_', ' ').capitalize()
            info.append(f"{show_field}: {getattr(self, field)}")

        return "\n".join(info)


class DBModel(BaseModel):

    def save(self):
        """Returns query to save or update model record in database"""
        if self.new:
            return {
                'collection': self.collection_name,
                'docs': self.to_dict(fields=self.save_fields),
                'method': 'insert'
            }
        else:
            self._modify()
            return {
                'collection': self.collection_name,
                'query': {'uuid': self.uuid},
                'docs': self.to_dict(fields=self.update_fields),
                'method': 'update'
            }

    def delete(self):
        """Returns query to delete model record from database"""
        return {
            'collection': self.collection_name,
            'query': {'uuid': self.uuid},
            'method': 'delete'
        }

    @classmethod
    def get(cls, uuid):
        """Returns instance by uuid"""
        return {
            'collection': cls.collection_name,
            'query': {'uuid': uuid}
        }

    @classmethod
    def filter(cls, values):
        """Returns query to filter data by values"""
        if not isinstance(values, dict):
            raise TypeError('values should be a dictionary type')

        return {
            'collection': cls.collection_name,
            'query': values
        }
