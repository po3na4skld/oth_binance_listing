
class BinanceHttpError(Exception):

    def __init__(self, data):
        super(BinanceHttpError, self).__init__()

        self.data = data


class DataBaseError(Exception):
    pass
