import logging
import json
import os

from telegram.ext import CallbackQueryHandler
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler
from telegram.ext import Updater
from telegram.ext import Filters

from models import Config
from base import MongoDB
from handlers import *


class BinanceListing:

    period2interval = {'T': 60, 'H': 3600, 'M': 2592000,
                       'D': 86400, 'W': 604800}

    order_regex = r"^/o_[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}\Z"
    deal_regex = r"^/d_[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}\Z"
    update_regex = r"new (stop loss|take profit|risk aversion|min capital) \d+.\d+"

    def __init__(self):
        self._config = None
        self.logging = self._setup_logging()

        handlers = [
            # general
            CommandHandler('start', start_handler, pass_user_data=True),
            CommandHandler('help', help_handler, pass_user_data=True),
            CommandHandler('enable', enable_handler, pass_user_data=True),
            CommandHandler('follow', follow_handler, pass_user_data=True),

            # account
            CommandHandler('account', account_handler, pass_user_data=True),
            CommandHandler('add_account', add_account, pass_user_data=True),
            CommandHandler('get_template', get_template, pass_user_data=True),
            CommandHandler('performance', performance_handler, pass_user_data=True),

            # order
            CommandHandler('last_order', last_order_handler, pass_user_data=True),
            CommandHandler('order_list', list_orders_handler, pass_user_data=True),
            CallbackQueryHandler(list_orders_callback, pattern='orders'),
            MessageHandler(Filters.regex(self.order_regex), last_order_handler, pass_user_data=True),

            # deal
            CommandHandler('last_deal', last_deal_handler, pass_user_data=True),
            CommandHandler('deal_list', list_deals_handler, pass_user_data=True),
            CallbackQueryHandler(list_deals_callback, pattern='deals'),
            MessageHandler(Filters.regex(self.deal_regex), last_deal_handler, pass_user_data=True),

            # manual
            CommandHandler('close_manual', close_manual_handler, pass_user_data=True),
            CallbackQueryHandler(close_manual_callback, pattern='close'),
            CallbackQueryHandler(close_choose_deal_callback, pattern='choose_close'),

            # limits
            MessageHandler(Filters.regex(self.update_regex), change_field_callback, pass_user_data=True),
            CommandHandler('check_news', check_news_callback, pass_user_data=True),

            # filter
            MessageHandler(Filters.video, document_filter),
            MessageHandler(Filters.document, document_filter),
            MessageHandler(Filters.photo, photo_filter),
            MessageHandler(Filters.text, text_filter),
            MessageHandler(Filters.voice, voice_filter, pass_user_data=True)
        ]

        self.updater = Updater(self.config.bot['token'], use_context=True)

        self.updater.bot.db = self._setup_database()
        self.updater.bot.logging = self.logging
        self.updater.bot.config = self.config

        self.updater.bot.job_queue = self.updater.job_queue

        self.updater.bot.job_queue.run_repeating(
            check_news_callback, interval=1500, first=3,
        )

        self.updater.bot.job_queue.run_repeating(
            check_limits_callback, interval=15, first=0
        )

        for handler in handlers:
            self.updater.dispatcher.add_handler(handler)

    @property
    def config(self):
        if not self._config:
            if not os.path.exists('config.json'):
                raise FileNotFoundError('Config file not found!')

            with open('config.json') as cfg:
                self._config = Config(**json.load(cfg))

        return self._config

    @staticmethod
    def _setup_logging():
        logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
        return logging.getLogger(__name__)

    def _setup_database(self):
        return MongoDB(**self.config.mongodb)

    def run(self):
        if self.updater:
            self.logging.info(msg=f"Bot successfully started.")
            self.updater.start_polling()
            self.updater.idle()
        else:
            raise AttributeError('Updater was configured wrong')


if __name__ == '__main__':
    app = BinanceListing()
    app.run()
