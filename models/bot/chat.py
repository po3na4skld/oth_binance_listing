class ChatModel:

    def __init__(self, update):
        try:
            kwargs = update.message.chat.__dict__
        except:
            kwargs = update.callback_query.message.chat.__dict__

        self.id = kwargs.get('id', '')


class DocumentModel:

    def __init__(self, update):
        kwargs = update.message.document.__dict__

        self.file_id = kwargs.get('file_id')
        self.file_unique_id = kwargs.get('file_unique_id')
        self.file_name = kwargs.get('file_name')
        self.mime_type = kwargs.get('mime_type')
        self.file_size = kwargs.get('file_size')


class MessageModel:

    def __init__(self, update):
        kwargs = update.message.__dict__

        self.id = kwargs.get('message_id', '')
        self.date = kwargs.get('date', '')
        self.text = kwargs.get('text', '')


class ContextModel:

    def __init__(self, context):
        self.context = context

    @property
    def bot(self):
        return self.context.bot

    def db(self):
        return self.context.bot.db
