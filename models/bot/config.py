class Config:

    def __init__(self, **kwargs):

        self.bot = kwargs.get('bot')
        self.exchange = kwargs.get('exchange')
        self.mongodb = kwargs.get('mongodb')

        self.is_enabled = kwargs.get('is_enabled', False)

        self.followers = kwargs.get('followers', [])
        self.admins = kwargs.get('admins', [])

        self.owner = 361362616

        self.currency_priority = kwargs.get('currency_priority')

    def enable(self):
        self.is_enabled = True

    def disable(self):
        self.is_enabled = False
