from base import DBModel


class UserModel(DBModel):

    save_fields = ['chat_id', 'username', 'first_name', 'last_name',
                   'account_uuid', 'account_type', 'is_follower',
                   'stop_loss', 'take_profit', 'key', 'secret']

    update_fields = ['username', 'first_name', 'last_name',
                     'account_uuid', 'account_type', 'is_follower',
                     'stop_loss', 'take_profit',
                     'key', 'secret']

    show_fields = ['username', 'first_name', 'last_name']

    collection_name = 'users'

    def __init__(self, **kwargs):
        super(UserModel, self).__init__(**kwargs)

        self.chat_id = kwargs.get('chat_id', kwargs.get('id', None))
        self.username = kwargs.get('username', '')

        self.first_name = kwargs.get('first_name', '')
        self.last_name = kwargs.get('last_name', '')

        self.account_uuid = kwargs.get('account_uuid', '')
        self.account_type = kwargs.get('account_type', '')

        self.is_follower = kwargs.get('is_follower', False)

        self.stop_loss = kwargs.get('stop_loss', None)
        self.take_profit = kwargs.get('take_profit', None)
        self.time_out = kwargs.get('time_out', None)

        self.key = kwargs.get('key', None)
        self.secret = kwargs.get('secret', None)

        if self.account_type and self.account_type not in ['spot', 'margin']:
            raise ValueError('Account type can be only spot or margin')

    @property
    def exchange(self):
        return {'key': self.key, 'secret': self.secret}

    @staticmethod
    def get_or_create(db, update):
        user = db.fetch_one(
            collection='users',
            query={'chat_id': update.message.chat.id}
        )

        if not user:
            user = UserModel(**update.message.chat.__dict__)
            db.execute(**user.save())
        else:
            user = UserModel(**user)

        return user

    def check_limits(self, price, order):
        if self.check_stop_loss(price, order):
            return True

        if self.check_take_profit(price, order):
            return True

        return False

    def check_stop_loss(self, price, order):
        if self.stop_loss:
            if order.side == 'BUY':
                limit = order.price - order.price * self.stop_loss
                return price <= limit

            elif order.side == 'SELL':
                limit = order.price + order.price * self.stop_loss
                return price >= limit

        return False

    def check_take_profit(self, price, order):
        if self.take_profit:
            if order.side == 'BUY':
                limit = order.price + order.price * self.take_profit
                return price >= limit

            elif order.side == 'SELL':
                limit = order.price - order.price * self.take_profit
                return price <= limit

        return False

    @classmethod
    def get_followers(cls):
        return {
            'collection': cls.collection_name,
            'query': {'is_follower': True}
        }
