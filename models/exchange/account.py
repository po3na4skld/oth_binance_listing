from base import DBModel
from .asset import SpotAsset, MarginAsset


class BaseAccount(DBModel):

    save_fields = ['user_uuid', 'started_capital', 'account_type',
                   'min_capital', 'risk_aversion']

    update_fields = ['min_capital', 'risk_aversion']

    show_fields = ['account_type', 'capital', 'profit', 'risk_aversion']

    collection_name = 'binance_accounts'

    def __init__(self, **kwargs):
        super(BaseAccount, self).__init__(**kwargs)

        self.user_uuid = kwargs.get('user_uuid', None)
        self.account_type = kwargs.get('account_type', None)

        if not self.account_type:
            raise ValueError('Account type cannot be empty')

        if self.account_type not in ['spot', 'margin']:
            raise ValueError('Wrong account type')

        self.started_capital = kwargs.get('started_capital', 0)
        self.min_capital = kwargs.get('min_capital', 0)
        self.risk_aversion = kwargs.get('risk_aversion', 0.1)

        self.capital = round(kwargs.get('capital', 0), 4)

        if self.account_type == 'spot':
            self.assets = list([SpotAsset(**i) for i in kwargs.get('balances', [])])
        else:
            self.assets = list([MarginAsset(**i) for i in kwargs.get('userAssets')])

    @property
    def profit(self):
        return round(self.capital - self.started_capital, 2)

    def check_balance(self):
        """Checks if balance bigger than minimum accepted value"""
        return self.capital > self.min_capital

    @property
    def not_null_balances(self):
        if self.account_type == 'spot':
            return list([i for i in self.assets if i.free])
        elif self.account_type == 'margin':
            return list([i for i in self.assets if i.free or i.borrowed])
        else:
            raise ValueError('Account type not supported')

    def order_size(self, asset):
        if not self.assets or asset not in [i.name for i in self.assets]:
            return 0

        amount = [i for i in self.assets if i.name == asset][0].free
        return amount * self.risk_aversion

    def is_overcharged(self, asset, price):
        if not self.assets or asset not in [i.name for i in self.assets]:
            return True

        amount = [i for i in self.assets if i.name == asset][0].free
        return amount < self.order_size(asset) / price


class SpotAccount(BaseAccount):

    _show_fields = ['maker_commission', 'taker_commission',
                    'buyer_commission', 'seller_commission',
                    'can_trade', 'can_withdraw', 'can_deposit']

    def __init__(self, **kwargs):
        _ = kwargs.pop('account_type', None)

        super(SpotAccount, self).__init__(account_type='spot', **kwargs)
        self.show_fields += self._show_fields

        self.maker_commission = float(kwargs.get('makerCommission', 0)) / 100
        self.taker_commission = float(kwargs.get('takerCommission', 0)) / 100

        self.buyer_commission = float(kwargs.get('buyerCommission', 0)) / 100
        self.seller_commission = float(kwargs.get('sellerCommission', 0)) / 100

        self.can_trade = kwargs.get('canTrade', False)
        self.can_withdraw = kwargs.get('canWithdraw', False)
        self.can_deposit = kwargs.get('canDeposit', False)


class MarginAccount(BaseAccount):

    _show_fields = ['trade_enabled', 'transfer_enabled', 'borrow_enabled',
                    'margin_level', 'btc_available']

    def __init__(self, **kwargs):
        _ = kwargs.pop('account_type', None)

        super(MarginAccount, self).__init__(account_type='margin', **kwargs)
        self.show_fields += self._show_fields

        self.trade_enabled = kwargs.get('tradeEnabled')
        self.transfer_enabled = kwargs.get('transferEnabled')
        self.borrow_enabled = kwargs.get('borrowEnabled')

        self.margin_level = float(kwargs.get('marginLevel'))

        self.btc_available = float(kwargs.get('totalAssetOfBtc'))
        self.net_btc_available = float(kwargs.get('totalNetAssetOfBtc'))
        self.btc_liability = float(kwargs.get('totalLiabilityOfBtc'))
