from base import BaseModel


class SpotAsset(BaseModel):

    save_fields = ['asset', 'free', 'locked']
    show_fields = ['asset', 'free', 'locked']

    def __init__(self, **kwargs):
        super(SpotAsset, self).__init__(**kwargs)

        self.asset = kwargs.get('asset')
        self.free = float(kwargs.get('free'))
        self.locked = float(kwargs.get('locked'))

    @property
    def name(self):
        return self.asset


class MarginAsset(SpotAsset):

    def __init__(self, **kwargs):
        super(MarginAsset, self).__init__(**kwargs)

        self.save_fields += ['borrowed', 'interest', 'net_asset']
        self.show_fields += ['borrowed', 'interest', 'net asset']

        self.borrowed = kwargs.get('borrowed')
        self.interest = kwargs.get('interest')
        self.net_asset = kwargs.get('netAsset')
