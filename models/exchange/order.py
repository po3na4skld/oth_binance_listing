from base import DBModel


class BinanceOrder(DBModel):

    save_fields = ['user_uuid', 'symbol', 'order_id', 'price',
                   'transaction_time', 'orig_qty', 'executed_qty', 'status',
                   'time_in_force', 'order_type', 'side', 'fills', 'cash_flow',
                   'average_price', 'tot_commission', 'commission_asset']

    update_fields = ['price', 'transaction_time', 'orig_qty', 'executed_qty',
                     'status', 'time_in_force', 'fills', 'cash_flow']

    show_fields = ['symbol', 'average_price', 'executed_qty', 'status',
                   'order_type', 'side', 'cash_flow', 'tot_commission']

    small_info_fields = ['symbol', 'status', 'side', 'cash_flow']

    collection_name = 'binance_orders'

    def __init__(self, **kwargs):
        super(BinanceOrder, self).__init__(**kwargs)

        self.user_uuid = kwargs.get('user_uuid', None)

        self.symbol = kwargs.get('symbol')
        self.order_id = kwargs.get('orderId', kwargs.get('order_id'))

        self.price = float(kwargs.get('price'))
        self.transaction_time = kwargs.get('transactTime',
                                           kwargs.get('transaction_time'))

        self.orig_qty = float(kwargs.get('origQty', kwargs.get('orig_qty')))
        self.executed_qty = float(kwargs.get('executedQty',
                                             kwargs.get('executed_qty')))

        self.status = kwargs.get('status')
        self.time_in_force = kwargs.get('timeInForce',
                                        kwargs.get('time_in_force'))

        self.order_type = kwargs.get('type', kwargs.get('order_type'))
        self.side = kwargs.get('side')
        self.fills = self._process_fills(kwargs.get('fills'))

    def get_small_info(self):
        info = []

        for field in self.small_info_fields:
            if getattr(self, field) is None:
                continue

            field = field.replace('_', ' ').capitalize()
            info.append(f"{field}: {getattr(self, field)}")

        info.append(f"/o_{self.uuid}")
        return "\n".join(info)

    @staticmethod
    def _process_fills(fills):
        return list([{
            'price': float(fill['price']),
            'qty': float(fill['qty']),
            'commission': float(fill['commission']),
            'commission_asset': fill.get('commissionAsset',
                                         fill['commission_asset'])
        } for fill in fills])

    @property
    def cash_flow(self):
        return self.executed_qty * self.average_price

    @property
    def average_price(self):
        if self.fills:
            return sum([i['price'] for i in self.fills]) / len(self.fills)
        else:
            return None

    @property
    def tot_commission(self):
        if self.fills:
            return sum([i['commission'] for i in self.fills])
        else:
            return None

    @property
    def commission_asset(self):
        if self.fills:
            return list(tuple([i['commission_asset'] for i in self.fills]))
        else:
            return None
