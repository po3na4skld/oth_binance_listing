from base import DBModel
from .order import BinanceOrder


class BinanceDeal(DBModel):

    save_fields = ['user_uuid', 'symbol', 'open_order', 'close_order',
                   'is_empty', 'is_filled', 'cash_flow', 'returns',
                   'is_profitable']

    update_fields = ['close_order', 'is_empty', 'is_filled', 'cash_flow',
                     'returns', 'is_profitable']

    show_fields = ['symbol', 'is_filled', 'cash_flow',
                   'returns', 'is_profitable']

    small_info_fields = ['symbol', 'is_filled', 'cash_flow']

    collection_name = 'binance_deals'

    def __init__(self, **kwargs):
        super(BinanceDeal, self).__init__(**kwargs)

        self.user_uuid = kwargs.get('user_uuid', None)

        self.symbol = kwargs.get('symbol', '')

        self.open_order = kwargs.get('open_order', {})
        self.close_order = kwargs.get('close_order', {})

        if self.open_order and not isinstance(self.open_order, BinanceOrder):
            self.open_order = BinanceOrder(**self.open_order)

        if self.close_order and not isinstance(self.close_order, BinanceOrder):
            self.close_order = BinanceOrder(**self.close_order)

    def get_small_info(self):
        info = []

        for field in self.small_info_fields:
            if getattr(self, field) is None:
                continue

            field = field.replace('_', ' ').capitalize()
            info.append(f"{field}: {getattr(self, field)}")

        info.append(f"/d_{self.uuid}")
        return "\n".join(info)

    @property
    def is_empty(self):
        return False if self.open_order else True

    def live_return(self, price):
        """Returns deal's returns in per cent even if it is not filled yet"""

        if self.is_empty:
            raise ValueError('You are trying to get returns from empty deal')

        if self.open_order.side == 'BUY':
            return (price - self.open_order.price) / self.open_order.price * 100
        else:
            return (self.open_order.price - price) / price * 100

    @property
    def is_filled(self):
        if isinstance(self.close_order, BinanceOrder):
            if isinstance(self.open_order, BinanceOrder):
                return True

        return False

    @property
    def cash_flow(self):
        if self.is_empty:
            raise ValueError('You are trying to get cash flow from empty deal')

        if self.is_filled:
            if self.open_order.side == 'BUY':
                cash_flow = self.close_order.cash_flow - self.open_order.cash_flow
            else:
                cash_flow = self.open_order.cash_flow - self.close_order.cash_flow
        else:
            if self.open_order.side == 'BUY':
                cash_flow = self.open_order.cash_flow
            else:
                cash_flow = -self.open_order.cash_flow

        return cash_flow

    @property
    def returns(self):
        return self.cash_flow / self.open_order.cash_flow * 100

    @property
    def is_profitable(self):
        return True if self.returns > 0 else False
