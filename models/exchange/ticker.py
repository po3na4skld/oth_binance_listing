from base import BaseModel


class Ticker(BaseModel):

    show_fields = ['bid', 'ask', 'spread', 'last_price']

    def __init__(self, **kwargs):
        super(Ticker, self).__init__(**kwargs)

        self.bid = float(kwargs.get('bidPrice'))
        self.ask = float(kwargs.get('askPrice'))
        self.last_price = float(kwargs.get('lastPrice'))

    @property
    def spread(self):
        return self.ask - self.bid
