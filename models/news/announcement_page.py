import hashlib

from bs4 import BeautifulSoup
import requests

from base import DBModel, BinanceHttpError


class AnnouncementPage(DBModel):

    base_url = 'https://www.binance.com'
    announcement_url = 'en/support/announcement/c-48?navId=48'

    save_fields = ['fingerprint', 'announcements', 'last_announcement']
    update_fields = ['fingerprint', 'announcements']
    collection_name = 'announcement_page'

    def __init__(self, **kwargs):
        super(AnnouncementPage, self).__init__(**kwargs)
        if not kwargs:
            kwargs = self._get_state()

        self.announcements = kwargs.get('announcements', [])
        self.fingerprint = kwargs.get('fingerprint', None)
        self.last_announcement = self.announcements[0]

    def _get_state(self):
        result = {'announcements': [], 'fingerprint': None}
        response = requests.get(f"{self.base_url}/{self.announcement_url}")

        if not response.ok:
            raise BinanceHttpError(
                **{
                    'code': response.status_code,
                    'text': response.text,
                    'json': response.json(),
                    'content': response.content
                }
            )

        soup = BeautifulSoup(response.content, 'lxml')
        for child in soup.recursiveChildGenerator():
            if child.name and child.name == 'a':
                if child.get('class', [''])[0] == 'css-1neg3js':
                    result['announcements'].append(child)

        result['announcements'] = self._process_announcements(result['announcements'])
        result['fingerprint'] = self._get_fingerprint(result['announcements'])

        return result

    @staticmethod
    def _get_fingerprint(announcements):
        if not announcements:
            return None

        return hashlib.sha256(str.encode(str(announcements))).hexdigest()

    def _process_announcements(self, announcements):
        if not announcements:
            return []

        return list([{'link': self.base_url + i.get('href'),
                      'preview_text': i.text} for i in announcements])
