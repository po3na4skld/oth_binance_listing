from datetime import datetime
import re

from bs4 import BeautifulSoup
import requests

from base import DBModel, BinanceHttpError


def to24hours(str_date):
    if str_date[-2:] == "AM" and str_date[:2] == "12":
        return "00" + str_date[2:-2]
    elif str_date[-2:] == "AM":
        return str_date[:-2]
    elif str_date[-2:] == "PM" and str_date[:2] == "12":
        return str_date[:-2]
    else:
        return str(int(str_date[:2]) + 12) + str_date[2:8]


class Announcement(DBModel):

    pairs_regex = r"[A-Z][A-Z0-9]+\/[A-Z][A-Z0-9]+"
    date_regex = r"\d{4}\/\d{2}\/\d{2} \d{1,2}:\d{2} \w{2}"

    save_fields = ['link', 'preview_text', 'title', 'pairs',
                   'full_text', 'publication_date', 'open_date',
                   'is_relevant']

    update_fields = ['is_relevant']

    show_fields = ['title', 'pairs', 'open_date']

    collection_name = 'announcements'

    def __init__(self, **kwargs):
        super(Announcement, self).__init__(**kwargs)

        self.link = kwargs.get('link', '')
        self.preview_text = kwargs.get('preview_text', '')

        if self.link:
            content = self.get_content()

            self.title = content['title']
            self.full_text = content['full_text']

            self.pairs = content['pairs']
            self.open_date = content['open_date']

            self.publication_date = content['publication_date']
            self.is_relevant = content['is_relevant']

    def get_content(self):
        if not self.link:
            raise ValueError('Link not specified')

        result = {'full_text': "", 'pairs': [], 'is_relevant': False,
                  'publication_date': None, 'title': '', 'open_date': None}

        response = requests.get(self.link)
        if not response.ok:
            raise BinanceHttpError(
                **{
                    'code': response.status_code,
                    'text': response.text,
                    'json': response.json(),
                    'content': response.content
                }
            )

        soup = BeautifulSoup(response.content, 'lxml')
        for i, child in enumerate(soup.recursiveChildGenerator()):
            if child.name and child.name == 'div':
                if child.get('class', [''])[0] == 'css-3fpgoh':
                    result['full_text'] += f"\n{child.text}"
                    if i == 3:
                        break

        result['pairs'] += self._get_pairs(result['full_text'])
        result['open_date'] = self._get_open_date(result['full_text'])

        result['title'] = soup.title.text if soup.title.text else ''
        result['publication_date'] = self._get_publication_date(soup)
        result['is_relevant'] = self._check_relevancy(result)

        return result

    @staticmethod
    def _get_publication_date(soup):
        date = ''
        for child in soup.recursiveChildGenerator():
            if child.name and child.name == 'div':
                if child.get('class', [''])[0] == 'css-f1q2g4':
                    date += child.text

        return datetime.strptime(date, "%Y-%m-%d %H:%M") if date else None

    def _get_pairs(self, text):
        return list([i.split('/') for i in re.findall(self.pairs_regex, text)])

    def _get_open_date(self, text):
        open_dates = list([datetime.strptime(
            to24hours(i).replace('/', '-'), "%Y-%m-%d %H:%M "
        )
                     for i in re.findall(self.date_regex, text)])

        return None if not open_dates else open_dates[-1]

    @staticmethod
    def check_relevancy(content):
        if not content['pairs']:
            return False

        if not content['open_date']:
            return False

        for pair in content['pairs']:
            if 'USD' in pair[0]:
                return False

            if 'DOWN' in pair[0]:
                return False

            if 'UP' in pair[0]:
                return False

        if 'Isolated Margin' in content['title']:
            return False

        if 'delist' in content['title'].lower():
            return False

        if 'stable' in content['title'].lower():
            return False

        if content['title'].startswith('Binance Adds'):
            return False

        if 'Innovation Zone' in content['title']:
            return True

        if 'Binance Launchpool' in content['title']:
            return True

        if content['title'].startswith('Introducing'):
            return True

        if content['title'].startswith('Binance Will List'):
            return True

        if content['title'].startswith('Binance Lists'):
            return True

        return False

    def __repr__(self):
        return str(self.to_dict())
