from models import ChatModel
from models import DocumentModel
from models import UserModel

from .utils import process_json_account


def photo_filter(update, context):
    chat = ChatModel(update)
    text = """I can't process photos yet."""
    return context.bot.send_message(chat.id, text)


def document_filter(update, context):
    chat, db = ChatModel(update), context.bot.db
    user = UserModel.get_or_create(db, update)

    if user.account_uuid:
        return context.bot.send_message(chat.id, 'You already have an account')
    document = DocumentModel(update)

    if document.mime_type != 'application/json':
        text = """I can't process documents yet."""
        return context.bot.send_message(chat.id, text)
    else:
        if 'account' in document.file_name:
            values = context.bot.get_file(document.file_id).download_as_bytearray()
            text = process_json_account(db, user, values)
            return context.bot.send_message(chat.id, text)


def voice_filter(update, context):
    chat = ChatModel(update)
    text = """I can't voice messages yet."""
    return context.bot.send_message(chat.id, text)


def text_filter(update, context):
    chat = ChatModel(update)
    text = "I can't process text messages yet."
    return context.bot.send_message(chat.id, text)
