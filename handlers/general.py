from models import ChatModel
from models import UserModel
from models import MessageModel

from .utils import get_account

from exchange import BinanceExchange


def start_handler(update, context):
    chat, db = ChatModel(update), context.bot.db
    user = UserModel.get_or_create(db, update)

    text = f"Hello, *{user.username}*, I'm your *Binance Listing* alerting bot.\n\n"
    text += 'I will monitor new binance listings and get you notified about them.\n\n'
    text += 'I will make orders for you on those listings automatically.\n\n'
    text += 'You can close active deals manually through me.\n\n'
    text += 'I will collect the performance of trading.\n\n'
    text += 'You can setup your stop losses and take profits through me.\n\n'
    text += '/help to see commands available and their description.\n\n'
    text += '\nHave a nice day, Bruh!'

    return context.bot.send_message(chat.id, text, parse_mode='markdown')


def help_handler(update, context):
    chat = ChatModel(update)

    text = 'Here is the list of most useful commands:\n\n'
    text += '/start, /help - Default commands with general information.\n'
    text += '/account - Get your account status and information.\n'
    text += '/performance - Get the performance of your account.\n\n'
    text += '/add_account - How to add your account.\n'
    text += '/get_template - Account template example.\n\n'
    text += '/last_order, /last_deal - Information about the latest order or deal.\n'
    text += '/deal_list, /order_list - Get lists of deals or orders.\n\n'
    text += '/close_manual - Close deal manually and instantly.\n\n'
    text += 'To update your stop loss or take profit goals send message like:\n'
    text += 'new take profit 0.1\nwhere the 0.1 is the new take profit and it '
    text += 'is equals to 10%. The same for stop loss, min capital, risk aversion '
    text += 'parameters.'

    return context.bot.send_message(chat.id, text)


def enable_handler(update, context):
    chat, db = ChatModel(update), context.bot.db
    config = context.bot.config
    user = UserModel.get_or_create(db, update)

    if user.chat_id not in config.admins:
        return context.bot.send_message(
            chat.id, "Sorry bruh, I'm not working with you"
        )

    if not config.is_enabled:
        config.enable()
    else:
        config.disable()

    text = f"I'm {'enabled' if config.is_enabled else 'disabled'} for now."
    context.bot.logging.info(msg=text)
    return context.bot.send_message(chat.id, text)


def change_field_callback(update, context):
    chat, db = ChatModel(update), context.bot.db
    user = UserModel.get_or_create(db, update)
    message = MessageModel(update)

    new_limit = float(message.text.split()[-1])
    attr = '_'.join(message.text.split()[1:-1])

    followers = [i.get('chat_id', '')
                 for i in db.fetch_all(**UserModel.get_followers())]

    if user.chat_id not in followers:
        return context.bot.send_message(
            chat.id, "Sorry bruh, I'm not working with you"
        )

    if attr in ['min_capital', 'risk_aversion']:
        if not user.account_uuid:
            return context.bot.send_message(
                chat.id, 'You should add your account first'
            )

        exchange = BinanceExchange(**user.exchange)
        account = get_account(db, exchange, user)

        setattr(account, attr, new_limit)
        db.execute(**account.save())

    if attr in ['take_profit', 'stop_loss']:
        setattr(user, attr, new_limit)
        db.execute(**user.save())

    text = f"Your {' '.join(message.text.split()[1:-1])} was set to {new_limit}"
    return context.bot.send_message(chat.id, text)


def follow_handler(update, context):
    chat, db = ChatModel(update), context.bot.db
    user = UserModel.get_or_create(db, update)

    user.is_follower = False if user.is_follower else True
    db.execute(**user.save())
    text = 'will' if user.is_follower else "won't"

    context.bot.send_message(chat.id, f'You {text} be notified about new announcements!')
