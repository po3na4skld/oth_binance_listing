from datetime import datetime

from models import UserModel
from models import BinanceDeal
from models import Announcement
from models import AnnouncementPage

from exchange import BinanceExchange

from .utils import get_account


def check_limits_callback(context, chat_id=None):
    db = context.bot.db
    config = context.bot.config

    if not config.is_enabled:
        return None

    if not chat_id:
        chat_id = config.owner

    user = db.fetch_one(collection='users', query={'chat_id': chat_id})
    user = UserModel(**user if user else {})
    if user.new or user.account_uuid:
        return None

    context.bot.logging.info(msg="Checking limits")
    exchange = BinanceExchange(**user.exchange)

    deals = db.fetch_all(
        collection='binance_deals',
        query={'user_uuid': user.uuid, 'is_filled': False},
    )

    if not deals:
        return None

    deals = [BinanceDeal(**i) for i in deals]
    for deal in deals:
        ticker = exchange.get_last_data(deal.symbol)
        price = ticker.ask if deal.open_order.side == 'SELL' else ticker.bid

        if user.check_limits(price, deal.open_order):
            deal, order = exchange.close_deal(deal)
            text = f"Deal was closed by {'stop loss' if deal.returns < 0 else 'take profit'}\n"
            text += deal.get_small_info()
            text += f'\nOrder info: /last_order\nDeal info /last_deal'

            db.execute(**deal.save())
            db.execute(**order.save())

            context.bot.send_message(user.chat_id, text)


def buy_coin(context, **kwargs):
    db = context.bot.db
    config = context.bot.config

    if not config.is_enabled:
        return None

    context.bot.logging.info(msg="Buying coin")

    chat_id = kwargs.get('chat_id', config.owner)

    user = db.fetch_one(collection='users', query={'chat_id': chat_id})
    user = UserModel(**user if user else {})
    if user.new or not user.account_uuid:
        return None

    exchange = BinanceExchange(**user.exchange)

    context.bot.logging.info(msg=f"Buying coins: {kwargs.get('pairs', [])}")
    account = get_account(db, exchange, user)

    pairs = kwargs.get('pairs')
    base_coins = [i.split('/')[-1] for i in pairs]
    best_pair, best_coin = None, None
    for coin in config.currency_priority:
        if coin in base_coins:
            idx = base_coins.index(coin)
            best_pair, best_coin = pairs[idx], coin
            break

    if not best_pair or not best_coin:
        return None

    size = account.order_size(best_coin)
    if not size:
        return context.bot.send_message(
            chat_id,
            f"Cannot perform deal: {best_pair}, not enough base coins"
        )

    ticker = exchange.get_last_data(best_pair)
    if account.is_overcharged(best_coin, ticker.ask):
        return context.bot.send_message(
            chat_id,
            f"Got overcharged on {best_coin} with order size: {size}"
        )

    count = size / ticker.ask
    order = exchange.create_order('buy', best_pair, count)

    text = "New order was opened!\n"
    text += str(order)
    context.bot.send_message(chat_id, text)

    deal = BinanceDeal(**{'user_uuid': user.uuid,
                          'symbol': best_pair, 'open_order': order})

    db.execute(**order.save())
    db.execute(**deal.save())


def check_news_callback(context):
    db = context.bot.db
    config = context.bot.config

    if not config.is_enabled:
        return None

    context.bot.logging.info(msg="Checking news")

    new_page = AnnouncementPage()

    previous_page = db.fetch_one(collection='announcement_page', query={})
    if previous_page:
        previous_page = AnnouncementPage(**previous_page)
        if new_page.fingerprint == previous_page.fingerprint:
            return None

    db.execute(**new_page.save())
    if previous_page:
        db.execute(**previous_page.delete())

    new_announcements = new_page.announcements
    if previous_page:
        new_announcements = [i for i in new_page.announcements
                             if i not in previous_page.announcements]

    followers = db.fetch_all(**UserModel.get_followers())
    for announcement in new_announcements:
        announcement = Announcement(**announcement)
        db.execute(**announcement.save())

        if not announcement.is_relevant:
            continue

        time_left = announcement.open_date - datetime.utcnow()

        # TODO: think about parallel processing
        for follower in followers:
            text = "There is new announcement!\n"
            text += str(announcement)
            text += f'\nYou have only {time_left.total_seconds() // 60} minutes before open!'

            context.bot.send_message(follower['chat_id'], text)

        context.bot.job_queue.run_once(
            buy_coin,
            job_kwargs={'pairs': announcement.pairs},
            when=time_left

        )
