import json

from models import SpotAccount
from models import MarginAccount

from exchange import BinanceExchange


def get_account(db, exchange, user):
    account = db.fetch_one(
        collection='binance_accounts',
        query={'user_uuid': user.uuid, 'account_type': user.account_type}
    )

    binance_account = exchange.get_account(user.account_type)
    if not account:
        account = binance_account
    else:
        account.update(binance_account)

    if user.account_type == 'spot':
        account = SpotAccount(**account)

    elif user.account_type == 'margin':
        account = MarginAccount(**account)

    return account


def process_json_account(db, user, values):
    required_fields = ['key', 'secret', 'min_capital', 'risk_aversion', 'account_type']

    try:
        account_info = values.decode('utf8').replace("'", '"')
        data = json.loads(account_info)
    except:
        return "Received bad file content"

    errors = []
    for field in required_fields:
        if field not in data:
            errors.append({field: 'field cannot be empty'})

    if data['account_type'] not in ['spot', 'margin']:
        errors.append({'account_type': 'field can be only spot or margin'})

    if not isinstance(data['risk_aversion'], float):
        errors.append({'risk_aversion': 'field should be float type'})

    if not (0 <= data['risk_aversion'] <= 1):
        errors.append({'risk_aversion': 'field should be in 0 to 1 range'})

    if data['min_capital'] < 0:
        errors.append({'min_capital': 'field should be greater than 0'})

    if errors:
        return json.dumps(errors)

    user.key = data['key']
    user.secret = data['secret']
    user.account_type = data['account_type']

    exchange = BinanceExchange(**user.exchange)
    try:
        binance_account = exchange.get_account(user.account_type)
    except:
        return "Invalid binance credentials"

    data.update(binance_account)

    if user.account_type == 'spot':
        account = SpotAccount(
            user_uuid=user.uuid,
            started_capital=binance_account['capital'],
            **data
        )
    else:
        account = MarginAccount(
            user_uuid=user.uuid,
            started_capital=binance_account['capital'],
            **data
        )

    user.account_uuid = account.uuid
    db.execute(**user.save())
    db.execute(**account.save())
    return 'Your account was added!'
