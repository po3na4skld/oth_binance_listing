from telegram import InlineKeyboardButton
from telegram import InlineKeyboardMarkup

from models import ChatModel
from models import UserModel
from models import BinanceOrder


def list_orders_callback(update, context):
    chat, db = ChatModel(update), context.bot.db
    user = UserModel.get_or_create(db, update)

    message_id = update.callback_query.message.message_id
    _, move, page = update.callback_query.data.split('_')

    followers = [i.get('chat_id', '')
                 for i in db.fetch_all(**UserModel.get_followers())]

    if user.chat_id not in followers:
        return context.bot.send_message(chat.id, "Sorry bruh, I'm not working with you")

    orders = db.fetch_all(
        collection='binance_orders',
        query={'user_uuid': user.uuid},
        sort=[('created', -1)],
        limit_size=5,
        page=int(page)
    )

    orders = [BinanceOrder(**i if i else {}) for i in orders]
    # TODO: find way to see the length of orders list
    if int(page) + 1 not in range(len([1, 1])):
        next_page = 0
    else:
        next_page = int(page) + 1

    text = '\n'.join([i.get_small_info() for i in orders])

    keyboard = InlineKeyboardMarkup([[InlineKeyboardButton('<< 5', callback_data=f'orders_prev_{int(page) - 1}'),
                                      InlineKeyboardButton('>> 5', callback_data=f'orders_next_{next_page}')]])

    return context.bot.edit_message_text(text, chat.id, message_id, reply_markup=keyboard)


def list_orders_handler(update, context):
    chat, db = ChatModel(update), context.bot.db
    user = UserModel.get_or_create(db, update)

    followers = [i.get('chat_id', '')
                 for i in db.fetch_all(**UserModel.get_followers())]

    if user.chat_id not in followers:
        return context.bot.send_message(chat.id, "Sorry bruh, I'm not working with you")

    orders = db.fetch_all(
        collection='binance_orders',
        query={'user_uuid': user.uuid},
        sort=[('created', -1)],
        limit_size=5,
        page=0
    )

    orders = [BinanceOrder(**i if i else {}) for i in orders]
    if not orders:
        return context.bot.send_message(chat.id, 'You have no orders yet.')

    text = '\n'.join([i.get_small_info() for i in orders])

    keyboard = InlineKeyboardMarkup([[InlineKeyboardButton('<< 5', callback_data='orders_prev_0'),
                                      InlineKeyboardButton('>> 5', callback_data='orders_next_1')]])

    return context.bot.send_message(chat.id, text, reply_markup=keyboard)


def last_order_handler(update, context):
    chat, db = ChatModel(update), context.bot.db
    user = UserModel.get_or_create(db, update)

    followers = [i.get('chat_id', '')
                 for i in db.fetch_all(**UserModel.get_followers())]

    if user.chat_id not in followers:
        return context.bot.send_message(chat.id, "Sorry bruh, I'm not working with you")

    if update.message.text.split('_'):
        order_code = update.message.text.split('_')[-1]
        order = db.fetch_one(**BinanceOrder.get(order_code))
    else:
        order = db.fetch_one(
            collection='binance_orders',
            query={'user_uuid': user.uuid},
            sort=[('created', -1)]
        )

    if not order:
        return context.bot.send_message(chat.id, 'You have no orders yet.')

    order = BinanceOrder(**order)
    return context.bot.send_message(chat.id, str(order))
