from telegram import InlineKeyboardButton
from telegram import InlineKeyboardMarkup

from models import ChatModel
from models import UserModel
from models import BinanceDeal


def list_deals_callback(update, context):
    chat, db = ChatModel(update), context.bot.db
    user = UserModel.get_or_create(db, update)

    message_id = update.callback_query.message.message_id
    _, move, page = update.callback_query.data.split('_')

    followers = [i.get('chat_id', '')
                 for i in db.fetch_all(**UserModel.get_followers())]

    if user.chat_id not in followers:
        return context.bot.send_message(chat.id, "Sorry bruh, I'm not working with you")

    deals = db.fetch_all(
        collection='binance_deals',
        query={'user_uuid': user.uuid},
        sort=[('created', -1)],
        limit_size=5,
        page=int(page)
    )

    deals = [BinanceDeal(**i if i else {}) for i in deals]
    # TODO: find way to see the length of deals list
    if int(page) + 1 not in range(len([1, 1])):
        next_page = 0
    else:
        next_page = int(page) + 1

    text = '\n'.join([i.get_small_info() for i in deals])

    keyboard = InlineKeyboardMarkup([[InlineKeyboardButton('<< 5', callback_data=f'deals_prev_{int(page) - 1}'),
                                      InlineKeyboardButton('>> 5', callback_data=f'deals_next_{next_page}')]])

    return context.bot.edit_message_text(text, chat.id, message_id, reply_markup=keyboard)


def list_deals_handler(update, context):
    chat, db = ChatModel(update), context.bot.db
    user = UserModel.get_or_create(db, update)

    followers = [i.get('chat_id', '')
                 for i in db.fetch_all(**UserModel.get_followers())]

    if user.chat_id not in followers:
        return context.bot.send_message(chat.id, "Sorry bruh, I'm not working with you")

    deals = db.fetch_all(
        collection='binance_deals',
        query={'user_uuid': user.uuid},
        sort=[('created', -1)],
        limit_size=5,
        page=0
    )

    deals = [BinanceDeal(**i if i else {}) for i in deals]
    if not deals:
        return context.bot.send_message(chat.id, 'You have no deals yet.')

    text = '\n'.join([i.get_small_info() for i in deals])

    keyboard = InlineKeyboardMarkup([[InlineKeyboardButton('<< 5', callback_data='deals_prev_0'),
                                      InlineKeyboardButton('>> 5', callback_data='deals_next_1')]])

    return context.bot.send_message(chat.id, text, reply_markup=keyboard)


def last_deal_handler(update, context):
    chat, db = ChatModel(update), context.bot.db
    user = UserModel.get_or_create(db, update)

    followers = [i.get('chat_id', '')
                 for i in db.fetch_all(**UserModel.get_followers())]

    if user.chat_id not in followers:
        return context.bot.send_message(chat.id, "Sorry bruh, I'm not working with you")

    if update.message.text.split('_'):
        deal_code = update.message.text.split('_')[-1]
        deal = db.fetch_one(**BinanceDeal.filter({'uuid': deal_code,
                                                  'user_uuid': user.uuid}))
    else:
        deal = db.fetch_one(
            collection='binance_deals',
            query={'user_uuid': user.uuid},
            sort=[('created', -1)]
        )

    if not deal:
        return context.bot.send_message(chat.id, 'You have no deals yet.')

    deal = BinanceDeal(**deal)
    return context.bot.send_message(chat.id, str(deal))
