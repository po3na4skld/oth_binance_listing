from base import get_metrics
from .utils import get_account

from models import ChatModel
from models import UserModel
from models import BinanceDeal

from exchange import BinanceExchange


def account_handler(update, context):
    chat, db = ChatModel(update), context.bot.db
    user = UserModel.get_or_create(db, update)
    exchange = BinanceExchange(**user.exchange)

    followers = [i.get('chat_id', '')
                 for i in db.fetch_all(**UserModel.get_followers())]

    if user.chat_id not in followers:
        return context.bot.send_message(chat.id, "Sorry bruh, I'm not working with you")

    if not user.account_uuid:
        return context.bot.send_message(chat.id, 'You need to add your account first.\nCheck /help command')

    account = get_account(db, exchange, user)

    text = f"Account information:\n\n{account}\n\n"
    text += "Last deal: /last_deal\nLast order: /last_order\n\n"
    text += "Deals list: /deal_list\nOrders list: /order_list"

    context.bot.logging.info(msg="Getting portfolio information")
    return context.bot.send_message(chat.id, text)


def performance_handler(update, context):
    chat, db = ChatModel(update), context.bot.db
    user = UserModel.get_or_create(db, update)
    exchange = BinanceExchange(**user.exchange)

    followers = [i.get('chat_id', '')
                 for i in db.fetch_all(**UserModel.get_followers())]

    if user.chat_id not in followers:
        return context.bot.send_message(chat.id, "Sorry bruh, I'm not working with you")

    if not user.account_uuid:
        return context.bot.send_message(chat.id, 'You need to add your account first.\nCheck /help command')

    deals = db.fetch_all(collection='binance_deals', query={'user_uuid': user.uuid})
    deals = list([BinanceDeal(**i if i else {}) for i in deals])

    account = get_account(db, exchange, user)
    metrics = get_metrics(deals, account.profit, pretty=True)

    text = f"Performance metrics:\n\n{metrics}\n\n"
    text += "Last deal: /last_deal\nLast order: /last_order\n\n"
    text += "Deals list: /deal_list\nOrders list: /order_list"

    context.bot.logging.info(msg="Getting portfolio performance")
    return context.bot.send_message(chat.id, text)


def add_account(update, context):
    chat = ChatModel(update)

    text = "To add you account, you should send the JSON file\n"
    text += "with the next parameters:\n"
    text += "1. key: Binance API key\n2. secret: Binance secret key\n"
    text += "3. min_capital: Minimum capital amount in USDT "
    text += "at which I should stop working\n"
    text += "4. risk_aversion: Risk aversion factor (amount of value should be) "
    text += "invested in one deal (in range from 0 to 1)\n"
    text += "5. account_type: Type of the Binance account (spot or margin)\n"
    text += "Load template: /get_template"

    return context.bot.send_message(chat.id, text)


def get_template(update, context):
    chat = ChatModel(update)

    with open('account_creation_template.json', 'rb') as f:
        return context.bot.send_document(
            chat.id, f, filename='account_creation_template.json'
        )
