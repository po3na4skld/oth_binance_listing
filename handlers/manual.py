from telegram import InlineKeyboardButton
from telegram import InlineKeyboardMarkup

from models import ChatModel
from models import UserModel
from models import BinanceDeal

from exchange import BinanceExchange


def close_manual_callback(update, context):
    chat, db = ChatModel(update), context.bot.db

    _, order_type, deal_uuid = update.callback_query.data.split(',')
    message_id = update.callback_query.message.message_id

    user = db.fetch_one(collection='users', query={'chat_id': chat.id})
    user = UserModel(**user if user else {})
    if user.new or user.account_uuid:
        return None

    deal = db.fetch_one(
        collection='binance_deals',
        query={'uuid': deal_uuid}
    )

    deal = BinanceDeal(**deal if deal else {})
    if deal.new:
        return context.bot.edit_message_text(
            'You have no deals to close', chat.id, message_id
        )

    exchange = BinanceExchange(**user.exchange)

    deal, order = exchange.close_deal(deal)
    text = f"Deal was closed by you\n"
    text += deal.get_small_info()
    text += f'\nOrder info: /last_order\nDeal info /last_deal'

    db.execute(**deal.save())
    db.execute(**order.save())

    context.bot.edit_message_text(text, chat.id, message_id)


def close_choose_deal_callback(update, context):
    chat, db = ChatModel(update), context.bot.db

    deal_uuid = update.callback_query.data.split(',')[-1]
    message_id = update.callback_query.message.message_id

    deal = db.fetch_one(
        collection='binance_deals',
        query={'uuid': deal_uuid}
    )

    deal = BinanceDeal(**deal if deal else {})
    if deal.new:
        return context.bot.send_message(chat.id, 'You have no deals to close')

    text = 'Your deal\n'
    text += str(deal)
    text += '\nDo you really want to close it?'

    order_type = 'sell' if deal.open_order.side == 'BUY' else 'buy'
    keyboard = InlineKeyboardMarkup(
        [[InlineKeyboardButton(
            'Yes', callback_data=f'close,{order_type},{deal.uuid}'
        ),
          InlineKeyboardButton(
              'No', callback_data=f'close,no_no,{deal.uuid}'
          )]]
    )

    return context.bot.edit_message_text(text, chat.id, message_id, reply_markup=keyboard)


def close_manual_handler(update, context):
    chat, db = ChatModel(update), context.bot.db
    user = UserModel.get_or_create(db, update)

    followers = [i.get('chat_id', '')
                 for i in db.fetch_all(**UserModel.get_followers())]

    if user.chat_id not in followers:
        return context.bot.send_message(chat.id, "Sorry bruh, I'm not working with you")

    if not user.account_uuid:
        return context.bot.send_message(chat.id, 'You need to add account first, check /help command')

    deals = db.fetch_all(
        collection='binance_deals',
        query={'user_uuid': user.uuid, 'is_filled': False},
        sort=[('created', -1)]
    )

    deals = [BinanceDeal(**i) for i in deals]
    if not deals:
        return context.bot.send_message(chat.id, 'You have no deals opened')

    text = "Which deal do you want to close?"
    markup = InlineKeyboardMarkup(
        [[InlineKeyboardButton(
            d.get_small_info(),
            callback_data=f'choose_close,{d.uuid}'
        ) for d in deals]]
    )

    return context.bot.send_message(chat.id, text, reply_markup=markup)
